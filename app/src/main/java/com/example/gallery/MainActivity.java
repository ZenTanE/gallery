package com.example.gallery;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    ArrayList<Picture> pictures = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.mainRecyclerView);

        initData();

        // TODO

    }

    private void initData() {

        for (int i = 1; i < 37; i++) {

            if (i < 10) {

                pictures.add(new Picture(("Pic0" + i), "https://joanseculi.com/images/img0" + i + ".jpg", "Description"));

            } else {

                pictures.add(new Picture(("Pic" + i), "https://joanseculi.com/images/img" + i + ".jpg", "Description"));

            }

        }

    }

}