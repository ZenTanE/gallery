package com.example.gallery;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gallery.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Picture> pictures = new ArrayList<>();

    public MyAdapter(Context context, ArrayList<Picture> pictures) {
        this.context = context;
        this.pictures = pictures;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Picasso.get().load(
                pictures.get(position).getUrl())
                .centerCrop()
                .fit().into(holder.dataImageView);

        holder.dataText.setText(
                pictures.get(position).getText().toString());

    }

    @Override
    public int getItemCount() {
        return pictures.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView dataImageView;
        private TextView dataText;

        public MyViewHolder(@NonNull View itemView) {

            super(itemView);
            dataImageView = itemView.findViewById(R.id.dataImageView);
            dataText = itemView.findViewById(R.id.dataText);

        }

    }

}
